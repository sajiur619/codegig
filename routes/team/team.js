const express = require("express");
const { get } = require("../admin/admin");

const router = express.Router();

router.get("/cto", async (req, res) => {
    res.render("team/cto", { layout: false });
});
router.get("/saju", async (req, res) => {
    res.render("team/saju", { layout: false });
});
router.get("/olid", async (req, res) => {
    res.render("team/olid", { layout: false });
});
router.get("/dipu", async (req, res) => {
    res.render("team/dipu", { layout: false });
});
router.get("/ceo", async (req, res) => {
    res.render("team/ceo", { layout: false });
});
router.get("/mizan", async (req, res) => {
    res.render("team/mizan", { layout: false });
});



module.exports = router;
