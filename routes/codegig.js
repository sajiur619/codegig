const express = require("express");
const router = express.Router();
const db = require("../config/database");
const Gig = require("../models/Gig");
const Sequelize = require("sequelize");
const Op = Sequelize.Op;

// Get gig list
router.get("/", (req, res) => {
    Gig.findAll()
        .then((gigs) =>
            res.render("index", {
                gigs,
            })
        )
        .catch((err) => res.render("error", { error: err }));

    // // Display add gig form
    // router.get("/add", (req, res) => res.render("add"));
});




module.exports = router;
